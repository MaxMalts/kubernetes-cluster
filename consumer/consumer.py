import pika

print("Starting consumer")
connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
channel = connection.channel()

channel.queue_declare(queue='woof')

channel.basic_consume(queue='woof', auto_ack=True, on_message_callback=lambda _, __, ___, body: print(f" [x] Received {body}"))

print('Waiting for messages')
channel.start_consuming()
