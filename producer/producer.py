import time
import pika

print("Starting producer")
connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
channel = connection.channel()

channel.queue_declare(queue='woof')

print("Starting woofing")
i = 0
while True:
    channel.basic_publish(exchange='', routing_key='woof', body=f'Woof {i} times')
    print(f" [x] Sent 'Woof {i} times'")
    
    time.sleep(5)
    i += 1
